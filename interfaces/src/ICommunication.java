import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ICommunication extends Remote
{
    //register data frame TimeHistory/Spectrum type
    void registerData(Object object) throws RemoteException;

    //return data lists which contains correct values
    List<Spectrum<Integer>> getSpectrumDataList() throws RemoteException;
    List<Spectrum<Integer>> getSpectrumDataList(String device) throws RemoteException;
    List<Spectrum<Integer>> getSpectrumDataList(String device, String description, long date) throws RemoteException;
    List<TimeHistory<Integer>> getTimeHistoryDataList() throws RemoteException;
    List<TimeHistory<Integer>> getTimeHistoryDataList(String device) throws RemoteException;
    List<TimeHistory<Integer>> getTimeHistoryDataList(String device, String description,
                                                      long date) throws RemoteException;

    //return data object which contains correct values
    Spectrum<Integer> getSpectrumData(int itemPosition) throws RemoteException;
    TimeHistory<Integer> getTimeHistoryData(int itemPosition) throws RemoteException;

    //save data object on server which contains correct values
    void saveSpectrumData(int itemPosition) throws RemoteException;
    void saveTimeHistoryData(int itemPosition) throws RemoteException;
}
