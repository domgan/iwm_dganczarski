import java.io.Serializable;

public abstract class Packet implements Serializable
{
    protected String device;
    protected String description;
    protected long date;

    public Packet(String device, String description, long date) {
        this.device = device;
        this.description = description;
        this.date = date;
    }

    public Packet() {
        this.device = "Default device";
        this.description = "Default description";
        this.date = 0;
    }

    @Override
    public String toString() {
        return "Packet{" +
                "device='" + device + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                '}';
    }

    public String getDevice() {
        return device;
    }

    public String getDescription() {
        return description;
    }

    public long getDate() {
        return date;
    }
}
