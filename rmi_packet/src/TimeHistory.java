public class TimeHistory<T> extends Sequence
{
    double sensitivity;

    public TimeHistory()
    {
      super();
      this.sensitivity = 0;
    }

    public TimeHistory(double sensitivity, int channelNr, String unit, double resolution, T[] buffer, String device, String description, long date) {
        super(channelNr, unit, resolution, buffer, device, description, date);
        this.sensitivity = sensitivity;
    }

    @Override
    public String toString() {
        return "TimeHistory{" +
                "sensitivity=" + sensitivity +
                '}' + "\n" +
                super.toString();
    }
}
