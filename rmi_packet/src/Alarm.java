public class Alarm extends Packet
{
    int channelNr;
    int treshold;
    int direction; //0 - dowolny, -1 - w dół, 1 - w górę

    public Alarm()
    {
        super();
        this.channelNr = 0;
        this.treshold = 0;
        this.direction = 0;
    }

    public Alarm(int channelNr, int treshold, int direction, String device, String description, long date)
    {
        super(device, description, date);
        this.channelNr = channelNr;
        this.treshold = treshold;
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Alarm{" +
                "channelNr=" + channelNr +
                ", treshold=" + treshold +
                ", direction=" + direction +
                '}' + "\n" +
                super.toString();
    }

}
