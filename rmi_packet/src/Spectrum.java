public class Spectrum<T>  extends Sequence
{
    int scaling; // 0 -> linear, 1 -> logarythmic

    public Spectrum()
    {
        super();
        this.scaling = 0;
    }

    public Spectrum(int scaling, int channelNr, String unit, double resolution, T[] buffer, String device, String description, long date)
    {
        super(channelNr, unit, resolution, buffer, device, description, date);
        this.scaling = scaling;
    }

    @Override
    public String toString() {
        return "Spectrum{" +
                "scaling=" + scaling +
                '}' + "\n" +
                super.toString();
    }

}
