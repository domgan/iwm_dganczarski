PROJEKT: \
https://gitlab.com/domgan/iwm_dganczarski_projekt
___
RMI: \
Należy uruchomić Server z modułu server, następnie Client z modułu client
i postępować wg. poleceń terminala.
___
UDP:

1. Należy uruchomić UdpServer, następnie UdpClient.

2. Wpisując w terminal cyfrę "1" wysyłany zostaje obiekt klasy Spectrum, 
wpisując "2" obiekt klasy TimeHistory.

3. Server odbiera obiekt i zapisuje go do pliku o nazwie: {device}\_{description}\_{date}.txt.

4. Następnie należy wpisać nazwę pliku bez rozszerzenia, który chcemy odebrać z serwera.

5. Serwer odbiera nazwe żądanego pliku, wczytuje go i odsyła do klienta.

6. Terminal klienta wyświetla toString() odebranego obiektu.