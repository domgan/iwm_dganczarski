package udp;

import packet.Packet;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpServer extends Thread {
    public static void main(String[] args) {

        DatagramSocket socket = null;
        try {
            // args contain message content and server hostname
            InetAddress host = InetAddress.getLocalHost();
            socket = new DatagramSocket(9876, host);
            byte[] buffer = new byte[1024];
            while (true) {
                DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                System.out.println("Waiting for request...");
                socket.receive(request);

                try {
                    Packet read = (Packet) Tools.deserialize(buffer);

                    // tworzenie pliku
                    String device = read.getDevice();
                    String description = read.getDescription();
                    Long date = read.getDate();

                    var f = new FileOutputStream(new File("src/udp/files/" + device + "_" + description + "_" + date + ".txt"));
                    var o = new ObjectOutputStream(f);
                    // Write objects to file
                    o.writeObject(read);
                    System.out.println("Object save to a file. \n");
                    o.close();
                    f.close();

                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                DatagramPacket soc = new DatagramPacket(buffer, buffer.length);
                socket.receive(soc);
                String file = new String(soc.getData(), 0, soc.getLength());

                try {
                    FileInputStream fi = new FileInputStream(new File("src/udp/files/" + file + ".txt"));
                    ObjectInputStream oi = new ObjectInputStream(fi);
                    // Read objects
                    var file_data = (Packet) oi.readObject();
                    System.out.println("Chosen object (" + file + ".txt) loaded and sent. \n");
                    oi.close();
                    fi.close();
                    byte[] data = Tools.serialize(file_data);
                    DatagramPacket reply = new DatagramPacket(data, data.length, request.getAddress(), request.getPort());
                    socket.send(reply);

                } catch (FileNotFoundException e) {
                    String nothing = "No such object!";
                    byte[] data2 = nothing.getBytes();
                    DatagramPacket reply = new DatagramPacket(data2, data2.length, request.getAddress(), request.getPort());
                    socket.send(reply);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            socket.close();
        }
    }
}