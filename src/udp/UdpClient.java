package udp;

import packet.Packet;
import packet.Spectrum;
import packet.TimeHistory;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class UdpClient
{
    public static void main(String[] args) throws InterruptedException {

        DatagramSocket socket = null;
        Packet packet = null;

        var in = new Scanner(System.in);
        System.out.println("Input \"1\" if you want to send Spectrum object " +
                "or \"2\" if you want to send TimeHistory object: ");
        String s = in.nextLine();

        // utworzenie obiektu klasy Spectrum i TimeHistory
        Integer[] pp = {2, 1, 3, 7};
        if (s.equals("1")) {
            packet = new Spectrum<>("urz3", "op3", 25L, 7,
                    "unit", "multimeter", pp, 5);
        } else if (s.equals("2")) {
            packet = new TimeHistory<>("urz3", "TH", 5L, 7,
                    "unit", "micrometer", pp, 3);
        }

        try {
            byte[] buffer = new byte[1024];
            var host = InetAddress.getLocalHost();
            int serverPort = 9876;
            socket = new DatagramSocket();

            byte[] data = Tools.serialize(packet);

            while (true) {
                if (data == null) {
                    throw new RuntimeException("data's null");
                } else {
                    var request = new DatagramPacket(data, data.length, host, serverPort);
                    socket.send(request);
                    System.out.println("Sent data. \n");

                    System.out.println("Input name of the file you want to recieve (ex. urz_op_15): ");
                    String file = in.nextLine();

                    // wczytanie wybranego w terminalu obiektu z pliku i odpowiedz do klienta
                    //String path = device + "_" + description + "_" + date + ".txt";
                    var soc = new DatagramSocket();
                    byte[] data2 = file.getBytes();
                    var pac = new DatagramPacket(data2, data2.length, host, serverPort);
                    soc.send(pac);
                    System.out.println("Sent data. \n");


                    var reply = new DatagramPacket(buffer, buffer.length);
                    System.out.println("Waiting for reply...");
                    socket.receive(reply); //get data from server
                    System.out.println("Got reply: ");
                }
                try
                {
                    Packet read = (Packet) Tools.deserialize(buffer);
                    System.out.println("Retrived data: " + read.toString() + "\n");
                }
                catch (ClassNotFoundException e)
                {
                    e.printStackTrace();
                }

                TimeUnit.MILLISECONDS.sleep(500);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            socket.close();
        }
    }
}
