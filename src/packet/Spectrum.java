package packet;

public class Spectrum<T> extends Sequence<T> {
    int scaling;

    public Spectrum(String device, String description, Long date,
                    int channelNr, String unit, String resolution, T[] buffer, int scaling) {
        super(device, description, date, channelNr, unit, resolution, buffer);
        this.scaling = scaling;
    }

    public Spectrum() {
        super();
        this.scaling = 0;
    }

    @Override
    public String toString() {
        return "Spectrum{" +
                "scaling='" + scaling + '\'' +
                '}' + super.toString();
    }
}
