package packet;

public class TimeHistory<T> extends Sequence<T> {
    double sensitivity;

    public TimeHistory(String device, String description, Long date, int channelNr,
                       String unit, String resolution, T[] buffer, double sensitivity) {
        super(device, description, date, channelNr, unit, resolution, buffer);
        this.sensitivity = sensitivity;
    }

    public TimeHistory() {
        super();
        this.sensitivity = 0.0;
    }

    @Override
    public String toString() {
        return "TimeHistory{" +
                "sensitivity=" + sensitivity +
                '}' + super.toString();
    }
}

