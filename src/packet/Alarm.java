package packet;

public class Alarm extends Packet {
    int channelNr;
    int threshold;
    int direction;

    public Alarm(String device, String description, Long date, int channelNr, int threshold, int direction) {
        super(device, description, date);
        this.channelNr = channelNr;
        this.threshold = threshold;
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Alarm{" +
                "channelNr=" + channelNr +
                ", threshold=" + threshold +
                ", direction=" + direction +
                '}' + super.toString();
    }
}
