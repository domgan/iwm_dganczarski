package packet;

import java.util.Arrays;

public abstract class Sequence <T> extends Packet {
    protected int channelNr;
    protected String unit;
    protected String resolution;
    protected T[] buffer;

    public Sequence(String device, String description, Long date, int channelNr, String unit,
                    String resolution, T[] buffer) {
        super(device, description, date);
        this.channelNr = channelNr;
        this.unit = unit;
        this.resolution = resolution;
        this.buffer = buffer;
    }

    public Sequence() {
        super();
        this.channelNr = 0;
        this.unit = "Default unit";
        this.resolution = "milimeter";
        this.buffer = null;
    }

    @Override
    public String toString() {
        return "Sequence{" +
                "channelNr=" + channelNr +
                ", unit='" + unit + '\'' +
                ", resolution=" + resolution +
                ", buffer=" + Arrays.toString(buffer) +
                '}' + super.toString();
    }
}
