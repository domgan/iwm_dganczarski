package packet;

import java.io.Serializable;

public abstract class Packet implements Serializable {
    protected String device;
    protected String description;
    protected Long date;

    public Packet(String device, String description, Long date)
    {
        this.device = device;
        this.description = description;
        this.date = date;
    }

    public Packet() {
        this.device = "urz1";
        this.description = "opis1";
        this.date = 10L;
    }

    public String getDevice() {
        return device;
    }

    public String getDescription() {
        return description;
    }

    public Long getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Packet{" +
                "device='" + device + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                '}';
    }
}
