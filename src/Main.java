import packet.Spectrum;
import packet.TimeHistory;
import packet.Alarm;

public class Main {
    public static void main(String[] args)
    {
        var Alarm1 = new Alarm("Jan", "Pawel", 15L, 1, 3, -1);
        System.out.println(Alarm1.toString());

        var TimeHistory1 = new TimeHistory<>("john", "paul", 17L, 0,
                "drugi", "ress", new String[]{"jeden", "dwa"}, .12);
        System.out.println(TimeHistory1.toString());

        var Spectrum1 = new Spectrum<>("xiaomi", "tak", 20L, 5,
                "UnIt", "res", new Integer[]{6, 8}, 0);
        System.out.println(Spectrum1.toString());
    }
}