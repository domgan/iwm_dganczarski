import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client
{
    ICommunication remoteObject;
    Registry reg; // the remote objects registry
    private final String hostname;

    public static void main(String[] args)
    {
        Client client = new Client("localhost");
        client.startClient();
    }

    public Client(String hostname)
    {
        this.hostname = hostname;
    }

    public void  startClient()
    {
        while(true)
        {
            try
            {
                reg = LocateRegistry.getRegistry(this.hostname);
                remoteObject = (ICommunication) reg.lookup("Server");

                switch (Tools.mainMenu()) {
                    case 1 -> Tools.registerFrameMenu(remoteObject);
                    case 2 -> Tools.getData(remoteObject);
                    case 3 -> {
                        System.out.println("Exit from program...");
                        return;
                    }
                    default -> System.out.println("Incorrect option");
                }
            }
            catch (RemoteException | NotBoundException e)
            {
                e.printStackTrace();
            }
        }
    }
}