import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.*;

public class Servant extends UnicastRemoteObject implements ICommunication
{
    public List<Spectrum<Integer>> spectrumContainer = new Vector<Spectrum<Integer>>();
    public List<TimeHistory<Integer>> timeHistoryContainer = new Vector<TimeHistory<Integer>> ();
    // creates a pool of threads for the Future to draw from
    public ExecutorService pool = Executors.newFixedThreadPool(20);

    public Servant() throws RemoteException
    {

    }

    @Override
    public void registerData(Object object)
    {
        new Thread(() -> {
            if(Tools.getClassType(object).equals("Spectrum"))
            {
                Tools.addSpectrumData((Spectrum<Integer>)object, spectrumContainer);
            }
            else if(Tools.getClassType(object).equals("TimeHistory"))
            {
                Tools.addTimeHistoryData((TimeHistory<Integer>)object, timeHistoryContainer);
            }
            else
            {
                System.out.println("[SERVER] Registration error!");
            }
        }).start();
    }

    //<-------------------------------------FILTER LISTS-------------------------------------
    @Override
    public List<Spectrum<Integer>> getSpectrumDataList() {
        Future<List<Spectrum<Integer>>> spectrumList = pool.submit(() -> spectrumContainer);
        try { return spectrumList.get(); }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Spectrum<Integer>> getSpectrumDataList(String deviceFilter) {
        Future<List<Spectrum<Integer>>> spectrumList = pool.submit(() ->
                Tools.getFilteredSpectrum(spectrumContainer, deviceFilter));
        try { return spectrumList.get(); }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Spectrum<Integer>> getSpectrumDataList(String deviceFilter, String descriptionFilter, long dateFilter) {
        Future<List<Spectrum<Integer>>> spectrumList = pool.submit(() ->
                Tools.getFilteredSpectrum(spectrumContainer, deviceFilter, descriptionFilter, dateFilter));
        try { return spectrumList.get(); }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<TimeHistory<Integer>> getTimeHistoryDataList() {
        Future<List<TimeHistory<Integer>>> timeHistoryFuture = pool.submit(() -> timeHistoryContainer);
        try { return timeHistoryFuture.get(); }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<TimeHistory<Integer>> getTimeHistoryDataList(String deviceFilter) {
        Future<List<TimeHistory<Integer>>> timeHistoryFuture = pool.submit(() ->
                Tools.getFilteredTimeHistory(timeHistoryContainer, deviceFilter));
        try { return timeHistoryFuture.get(); }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<TimeHistory<Integer>> getTimeHistoryDataList(String deviceFilter,
                                                             String descriptionFilter, long dateFilter) {
        Future<List<TimeHistory<Integer>>> timeHistoryFuture = pool.submit(() ->
                Tools.getFilteredTimeHistory(timeHistoryContainer, deviceFilter, descriptionFilter, dateFilter));
        try { return timeHistoryFuture.get(); }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
            return null;
        }
    }
    //<------------------------------------------------------------------------------------

    @Override
    public Spectrum getSpectrumData(int itemPosition) {
        Future<Spectrum<Integer>> spectrumFuture = pool.submit(() -> spectrumContainer.get(itemPosition));
        try { return spectrumFuture.get(); }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public TimeHistory<Integer> getTimeHistoryData(int itemPosition) {
        Future<TimeHistory<Integer>> timeHistoryFuture = pool.submit(() -> timeHistoryContainer.get(itemPosition));
        try { return timeHistoryFuture.get(); }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void saveSpectrumData(int itemPosition) {
        Future<Boolean> booleanFuture = pool.submit(() -> Tools.savePacketToFile(spectrumContainer.get(itemPosition)));
        try {
            booleanFuture.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void saveTimeHistoryData(int itemPosition) {
        Future<Boolean> booleanFuture = pool.submit(() ->
                Tools.savePacketToFile(timeHistoryContainer.get(itemPosition)));
        try {
            booleanFuture.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            e.printStackTrace();
        }
    }
}
