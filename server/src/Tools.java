import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

public class Tools extends Thread
{
    public static byte[] serialize(Object obj) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(obj);
            out.flush();
            return bos.toByteArray();
        }
        catch(IOException e) {
            e.printStackTrace();
            return null;
        }
        finally {
            try {
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean savePacketToFile(Packet packet)
    {
        try
        {
            String file_name = createFileName(packet);
            OutputStream os = new FileOutputStream(file_name);
            os.write(Objects.requireNonNull(serialize(packet)));
            os.close();
            System.out.println("[Tools] Successful save data to: " + file_name);
            return true;
        }
        catch (IOException e)
        {
            System.out.println("[Tools] Problem during saving data process: " + e.toString());
            return false;
        }
    }

    public static String createFileName(Packet packet)
    {
        String file_format;
        if(getClassType(packet).equals("Spectrum"))
        {
            file_format = ".spc";
        }
        else if(getClassType(packet).equals("TimeHistory"))
        {
            file_format = ".thi";
        }
        else
        {
            file_format = ".txt";
        }
        return packet.getDevice() + "_" + packet.getDescription() + "_" + packet.getDate() + file_format;
    }

    public static List<Spectrum<Integer>> getFilteredSpectrum(List<Spectrum<Integer>> container,String device)
    {
        List<Spectrum<Integer>> spectrumList = new Vector<>();
        for (Spectrum<Integer> integerSpectrum : container) {
            if (integerSpectrum.getDevice().equals(device)) {
                spectrumList.add(integerSpectrum);
            }
        }
        return spectrumList;
    }

    public static List<Spectrum<Integer>> getFilteredSpectrum(List<Spectrum<Integer>> container,String device,
                                                              String description, long date)
    {
        List<Spectrum<Integer>> spectrumList = new Vector<>();
        for (Spectrum<Integer> integerSpectrum : container) {
            if (integerSpectrum.getDevice().equals(device) || integerSpectrum.getDescription().equals(description) ||
                    integerSpectrum.getDate() == date) {
                spectrumList.add(integerSpectrum);
            }
        }
        return spectrumList;
    }

    public static List<TimeHistory<Integer>> getFilteredTimeHistory(List<TimeHistory<Integer>> container,String device)
    {
        List<TimeHistory<Integer>> timeHistoryList = new Vector<>();
        for (TimeHistory<Integer> integerTimeHistory : container) {
            if (integerTimeHistory.getDevice().equals(device)) {
                timeHistoryList.add(integerTimeHistory);
            }
        }
        return timeHistoryList;
    }

    public static List<TimeHistory<Integer>> getFilteredTimeHistory(List<TimeHistory<Integer>> container,
                                                                    String device, String description, long date)
    {
        List<TimeHistory<Integer>> timeHistoryList = new Vector<>();
        for (TimeHistory<Integer> integerTimeHistory : container) {
            if (integerTimeHistory.getDevice().equals(device) ||
                    integerTimeHistory.getDescription().equals(description) || integerTimeHistory.getDate() == date) {
                timeHistoryList.add(integerTimeHistory);
            }
        }
        return timeHistoryList;
    }

    public static void addSpectrumData(Spectrum<Integer> newSpectrum, List<Spectrum<Integer>> container)
    {
        container.add(newSpectrum);
        System.out.println("---");
        System.out.println("[SERVER] Registered new <Spectrum> data:");
        System.out.println(newSpectrum);
        System.out.println("---");
    }

    public static void addTimeHistoryData(TimeHistory<Integer> newTimeHistory, List<TimeHistory<Integer>> container)
    {
        container.add(newTimeHistory);
        System.out.println("---");
        System.out.println("[SERVER] Registered new <TimeHistory> data:");
        System.out.println(newTimeHistory);
        System.out.println("---");
    }

    public static String getClassType(Object object)
    {
        return object.getClass().getTypeName();
    }

}

